let prom = true;
while (prom) {
  var userPrompt = prompt("Rock, Paper or Scissors?: ");
  userPrompt = userPrompt.toLowerCase()
  if (userPrompt === 'paper' || userPrompt === 'rock' || userPrompt === 'scissors') {
    prom = false
  }
}

let ran = Math.floor((Math.random() * 3) +1);
let computerChoice

if (ran===1){
  computerChoice = 'rock';
}
else if (ran===2){
  computerChoice = 'paper';
}
else if (ran===3){
  computerChoice = 'scissors';
}
console.log(userPrompt)
console.log(computerChoice)

determineWinner (userPrompt, computerChoice);


function createNewDiv(content){
  let newDiv= document.createElement('div');
  let textNode= document.createTextNode(content)
  newDiv.appendChild(textNode);
  let destination = document.getElementById('d1');
  destination.appendChild(newDiv);
}

function determineWinner(userChoice, computerChoice) {
  if (userChoice === computerChoice) {
    createNewDiv('The game is a tie')
  } 
    
  else if (userChoice === 'rock') {
    if (computerChoice === 'paper') {
      createNewDiv('Computer won')
    } else {
      createNewDiv('Woo! You won!')
  } 
  }

  else if (userChoice === 'scissors') {
    if (computerChoice === 'rock') {
      createNewDiv('Computer won')
    } else {
      createNewDiv('Woo! You won!')
    }
  } 
  else if (userChoice === 'paper') {
    if (computerChoice === 'scissors') {
      createNewDiv('Computer won')
    } else {
      createNewDiv('Woo! You won!')
    }
  }
 
  let newDiv= document.createElement('button');
  newDiv.id = 'tryAgain'
  newDiv.type = 'submit'
  newDiv.addEventListener('click', function () { 
    location.reload()
  })
  let textNode= document.createTextNode('Try Again?')
  newDiv.appendChild(textNode);
  let destination = document.getElementById('d1');
  destination.appendChild(newDiv);
}
